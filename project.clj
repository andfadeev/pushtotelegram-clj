(defproject telegrammer-clj "0.1.0-SNAPSHOT"
  :description "PushToTelegram"
  :url "http://www.pushtotelegram.com/docs"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.reader "1.0.0-alpha2"]
                 [clj-http "2.0.0"]
                 [hiccup "1.0.5"]
                 [ring "1.4.0"]
                 [compojure "1.4.0"]
                 [mount "0.1.7"]
                 [meta-merge "0.1.1"]
                 [com.zaxxer/HikariCP "2.4.4"]
                 [honeysql "0.6.3"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 [http-kit "2.1.18"]
                 [com.stuartsierra/component "0.3.1"]
                 [org.postgresql/postgresql "9.2-1004-jdbc4"]
                 [org.clojure/tools.logging "0.3.1"]
                 [ch.qos.logback/logback-classic "1.1.3"]
                 [buddy/buddy-auth "0.8.2"
                  :exclusions [org.clojure/tools.reader]]
                 [http-kit "2.1.19"]
                 [org.clojure/core.async "0.2.374"
                  :exclusions [org.clojure/tools.reader]]
                 [ring/ring-defaults "0.1.5"]
                 [ring/ring-json "0.4.0"]
                 [ring/ring-anti-forgery "1.0.0"]
                 [migratus "0.8.8"]
                 [clj-time "0.11.0"]
                 [org.clojure/java.jdbc "0.4.2"]
                 [korma "0.4.0"]
                 [metosin/compojure-api "1.0.2"]
                 [prismatic/schema "1.0.4"]
                 [bouncer "1.0.0"]
                 [environ "1.0.2"]
                 [com.cloudinary/cloudinary-http44 "1.3.0"]]
  :profiles {:uberjar {:main telegrammer-clj.core
                       :aot :all}
             :server {:main telegrammer-clj.core}
             :dev [:project/dev :profiles/dev]
             :profiles/dev {}
             :project/dev {:dependencies [[org.clojure/tools.namespace "0.2.11"]]
                           :source-paths ["dev"]}}
  :plugins [[lein-environ "1.0.2"]]
  :aliases {"migrate" ["run" "-m" "telegrammer-clj.migrations/migrate"]
            "rollback" ["run" "-m" "telegrammer-clj.migrations/rollback"]})
