(ns dev
  (:require [cider.nrepl.middleware.stacktrace :refer [wrap-stacktrace]]
            [clojure.tools.namespace.repl :refer [refresh]]
            [com.stuartsierra.component :as component]
            [meta-merge.core :refer [meta-merge]]
            [telegrammer-clj.system :as system]
            [telegrammer-clj.config :as config]
            [clojure.edn :as edn]))

(def system nil)

(def dev-config
  {:site-app {:middleware [wrap-stacktrace]}})

(def config
  (meta-merge config/defaults
              config/environ
              dev-config))

(defn init []
  (alter-var-root #'system
                  (constantly (system/build-system config))))

(defn start []
  (alter-var-root #'system component/start))

(defn stop []
  (alter-var-root #'system
                  (fn [s] (when s (component/stop s)))))

(defn go []
  (init)
  (start))

(defn reset []
  (stop)
  (refresh :after 'dev/go))

;; helpers

(defn db []
  (:spec (:db system)))
