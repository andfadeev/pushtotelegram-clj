(ns telegrammer-clj.route.doc-route
  (:require [compojure.core :refer :all]
            [telegrammer-clj.view.layout :as l]
            [telegrammer-clj.view.nav :as menu]))

(defn docs-page [request]
  (let [user (get-in request [:session :user])]
    (l/with-container
      (menu/nav :docs user)
      [:div.row
        [:div.col.s12.m9.l10
         [:div#about.section.scrollspy
          [:h4 "About"]
          [:p "PushToTelegram is a simple service to notify your users with Telegram messenger. It is currently in development and is completly free. "]]
         [:div#create-account.section.scrollspy
          [:h4 "Create account"]
          [:p "You can create account with one of this social networks: Google+, Facebook. With account you can subscribe to channels or create channel."]
          [:a {:href "/login" :class "waves-effect waves-light btn white blue-text"} "Create account"]]
         [:div#link-telegram.section.scrollspy
          [:h4 "Link to Telegram"]
          [:p "To receive notifications you should link your account with Telegram messenger. To do it, find our bot " [:a {:href "https://web.telegram.org/#/im?p=@PushToTelegramBot"} "@PushToTelegramBot"] " in Telegram or " [:a {:href "https://web.telegram.org/#/im?p=@PushToTelegramBot"} "follow this link."] " Then your will receive notifications from subscribed channels."]]
         [:div#create-account.section.scrollspy
          [:h4 "Create channel"]
          [:p "Once you have account, you can create channels. The can be private or public (default). Private channels are visible only for owner. Now you can send messages to channel with our public API (or manually), and PushToTelegram service will send this message to all subscribers."]
          [:a {:href "/new-channel" :class "waves-effect waves-light btn white blue-text"} "Create channel"]]
         [:div#create-account.section.scrollspy
          [:h4 "Public API"]
          [:p "Once you have channel, there is API key on channel page and you can use our public API with this key. Swagger is used for API docs."]
          [:a {:href "/api-docs" :target "_blank" :class "waves-effect waves-light btn white blue-text"} "See API docs"]
          ;; [:pre
          ;;  [:code "GET http://alfa-pushtotelegram.rhcloud.com/api/sendMessage?key=api_key&type=channel&title=title&text=text&link=link"]]
          ;; [:ul
          ;;  [:li [:b "key"] " - your channel api key " [:i "(Required)"]]
          ;;  [:li [:b "type"] " - type of notification " [:i "(Required)"]
          ;;   [:ul
          ;;    [:li [:i "channel"] " - send message to all channel subscribers"]
          ;;    ;;[:li [:i "personal"] " - send personal message to one subscriber"]
          ;;    ]]
          ;;  [:li [:b "title"] " - title of message " [:i "(Required)"]]
          ;;  [:li [:b "text"] " - text of message " [:i "(Required)"]]
          ;;  [:li [:b "link"] " - link added to message " [:i "(Optional)"]]
          ;;  ;;[:li [:b "user_id"] " - message reciever id " [:i "(Required for personal message type)"]]
          ;;  ]
          ]]
        [:div.col.hide-on-small-only.m3.l2
         [:ul.section.table-of-contents
          [:li [:a {:href "#about"} "About"]]
          [:li [:a {:href "#create-account"} "Create account"]]
          [:li [:a {:href "#link-telegram"} "Link Telegram"]]
          [:li [:a {:href "#create-channel"} "Create channel"]]
          [:li [:a {:href "#public-api"} "Public API"]]]]]
      {:title "PushToTelegram - Docs"
       :description "PushToTelegram is a simple service to notify your users with Telegram messenger. It is currently in development and is completly free."})))

