(ns telegrammer-clj.route.api-route
  (:require [bouncer
             [core :as bouncer]
             [validators :as v]]
            [clojure.walk :refer [keywordize-keys]]
            [ring.util
             [http-response :refer [ok]]
             [response :as response]]
            [schema.core :as s]
            [telegrammer-clj.bot.message-listener :as message-listener]
            [telegrammer-clj.model
             [channel-model :as channel-model]
             [tg-chat-model :as tg-chat-model]]
            [telegrammer-clj.schema.message-schema :as message-schema]))

(defn send-channel-message [db notif-chan storage-chan message]
  (let [channel-id (:channel_id (channel-model/by-api-key db (:key message)))
        chat-ids (->> (tg-chat-model/by-channel-id db channel-id)
                      (map :chat_id)
                      set)
        channel-message {:message (dissoc message :key)
                         :channel-id channel-id
                         :type :channel-message}]
    (doseq [chat-id chat-ids]
      (message-schema/put-notification-worker-message! notif-chan
                                                       (assoc channel-message :chat-id chat-id)))
    (message-schema/put-storage-worker-message! storage-chan channel-message)
    (ok {:channel-id channel-id
         :recipients-count (count chat-ids)})))
