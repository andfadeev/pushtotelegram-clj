(ns telegrammer-clj.route.channel-route
  (:require [bouncer
             [core :as bouncer]
             [validators :as v]]
            [clojure.string :as str]
            [environ.core :as e]
            [ring.util.response :as response]
            [telegrammer-clj.model
             [account-model :as account-model]
             [channel-model :as channel-model]
             [message-model :as message-model]
             [subscriber-model :as subscriber-model]]
            [telegrammer-clj.service.channel-service :as channel-service]
            [telegrammer-clj.view
             [channel-view :as channel-view]
             [layout :as view]
             [nav :refer [nav]]])
  (:import [com.cloudinary Cloudinary Transformation]))

(defn new-channel-page [request & [old-form]]
  (let [user (get-in request [:session :user])]
    (view/with-container
      (nav user)
      (channel-view/create-channel-form old-form))))

(defn- cb->bool [val]
  (if val true false))

(defn validate-channel-form [channel-form]
  (bouncer/validate
   channel-form
   [:file :size] [[v/in-range [0 256000] :message "Image size should be less then 256Kb"]]
   :name [[v/required :message "Channel name is required"]]
   :info [[v/required :message "Channel description is required"]]))

(defn valid-channel-form? [channel-form]
  (nil? (first (validate-channel-form channel-form))))

(defn create-channel
  [db {{:strs [channel-name
               channel-info
               channel-links
               is-private
               channel-image]} :multipart-params :as request}]
  (let [channel-form {:name channel-name
                      :info channel-info
                      :file channel-image
                      :links channel-links}]
    (if (valid-channel-form? channel-form)
      (let [user-id (:identity request)
            cloudinary-response (when-not (empty? (get-in channel-form [:file :filename]))
                                  (.upload (.uploader (Cloudinary. {"cloud_name" (e/env :cloudinary-cloud-name)
                                                                    "api_key" (e/env :cloudinary-api-key)
                                                                    "api_secret" (e/env :cloudinary-api-secret)}))
                                           (:tempfile channel-image)
                                           {"transformation" (doto (Transformation.)
                                                               (.crop "crop")
                                                               (.width 512)
                                                               (.height 512))}))
            channel-image-url (or (get cloudinary-response "url")
                                  (e/env :default-channel-image-url))
            channel {:account_id user-id
                     :name channel-name
                     :info channel-info
                     :links channel-links
                     :image_url channel-image-url
                     :is_private (cb->bool is-private)}]
        (let [channel-id (:channel_id (channel-service/create-channel-new db channel))]
          (response/redirect (str "/channel/" channel-id))))
      (new-channel-page request (validate-channel-form channel-form)))))

(defn channels-page [db request]
  (let [user (get-in request [:session :user])
        channels (channel-model/only-public db)]
    (channel-view/channels-page user channels)))

(defn my-channels-page [db request]
  (let [user (get-in request [:session :user])
        owned-channels (channel-model/by-owner-id db (:account_id user))
        subscribed-channels (channel-model/by-subscriber-id db (:account_id user))]
    (channel-view/my-channels-page user owned-channels subscribed-channels)))

(defn channel-page [db channel-id request]
  (let [user (get-in request [:session :user])
        channel (channel-model/by-channel-id db channel-id)
        links (when (seq (:links channel)) (map str/trim (-> channel
                                                             :links
                                                             (str/split #","))))
        messages (message-model/by-channel-id db channel-id)
        subscribers (account-model/subscribers db channel-id)]
    (channel-view/channel-page channel links subscribers messages user)))

(defn subscribe [db channel-id request]
  (let [user (get-in request [:session :user])]
    (when-not (subscriber-model/subscribed? db channel-id (:account_id user))
      (subscriber-model/subscribe db (:account_id user) channel-id))
    (response/redirect (str "/channel/" channel-id))))

(defn unsubscribe [db channel-id request]
  (let [user (get-in request [:session :user])]
    (when (subscriber-model/subscribed? db channel-id (:account_id user))
      (subscriber-model/unsubscribe! db (:account_id user) channel-id))
    (response/redirect (str "/channel/" channel-id))))
