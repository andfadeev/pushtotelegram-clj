(ns telegrammer-clj.route.tg-chat-route
  (:require [telegrammer-clj.model.tg-chat-model :as tg-chat-model]
            [ring.util.response :as response]
            [telegrammer-clj.view.layout :as l]
            [telegrammer-clj.view.nav :as nav]))

(defn link-account-to-chat [db chat-id request]
  (let [account-id (get-in request [:session :user :account_id])]
    (if (tg-chat-model/exists? db chat-id account-id)
      (response/redirect "already-linked")
      (do
        (tg-chat-model/create! db chat-id account-id)
        (response/redirect "/linked")))))

(defn linked-page [request]
  (let [user (get-in request [:session :user])]
    (l/with-container
      (nav/nav user)
      [:div.row
       [:div.col.s12
        [:h4 "You have successfully linked your telegram account!"]
        [:p [:a {:href "/"} "Go to main page"]]]])))

(defn already-linked-page [request]
  (let [user (get-in request [:session :user])]
    (l/with-container
      (nav/nav user)
      [:div.row
       [:div.col.s12
        [:h4 "You have already linked this telegram account!"]
        [:p [:a {:href "/" :class "btn"} "Go to main page"]]]])))
