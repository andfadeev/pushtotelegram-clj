(ns telegrammer-clj.route.index-route
  (:require [telegrammer-clj.model.channel-model :as channel-model]
            [telegrammer-clj.view
             [channel-view :refer [channels-two-col-block]]
             [layout :as view]
             [nav :as menu]]))

(defn index-page [db request]
  (view/base-layout
   (menu/nav (get-in request [:session :user]))
   [:div#index-banner.section.no-pad-bot
    [:div.container
     [:div.row
      [:br]
      [:br]
      [:div.col.s12.center-align
       [:h1 "PushToTelegram" [:small.red-text.tooltipped {:data-position "top"
                                                          :data-delay "50"
                                                          :data-tooltip "Service is in development now! It is alpha version and is completely free!"} ".alpha"]]
       [:h5 "A simple service to notify your users with Telegram messenger"]
       [:br]
       [:a.waves-effect.waves-light.btn-large {:href "/docs" :style "margin: 10px;"} "Get started"]
       [:a.waves-effect.waves-light.btn-large.white.teal-text {:href "/new-channel" :style "margin: 10px;"} "CREATE CHANNEL"]]]
     [:br]
     [:br]
     (channels-two-col-block "Popular channels" (channel-model/only-public db))
     [:div.fixed-action-btn {:style "bottom: 45px; right: 24px;"}
      [:a.btn-floating.btn-large.waves-effect.waves-light.red.tooltipped
       {:href "/new-channel"
        :data-position "left"
        :data-delay "50"
        :data-tooltip "Create new channel"}
       [:i.material-icons "add"]]]]]))
