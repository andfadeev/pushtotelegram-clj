(ns telegrammer-clj.route.message-route
  (:require [bouncer
             [core :as bouncer]
             [validators :as v]]
            [buddy.auth :as buddy-auth]
            [ring.util.response :as response]
            [telegrammer-clj.schema.message-schema :refer [put-notification-worker-message!]]
            [telegrammer-clj.model
             [channel-model :as channel-model]
             [tg-chat-model :as tg-chat-model]]
            [telegrammer-clj.view.message-view :as message-view]))

(defn validate-message-form [message]
  (bouncer/validate
   message
   :title [[v/required :message "Message title is required"]
           [v/max-count 64 :message "Message title is longer than 64 chars"]]
   :text [[v/required :message "Message text is required"]
          [v/max-count 512 :message "Message text is longer than 512 chars"]]
   :link [[v/max-count 128 :message "Links are longer than 128 chars"]]))

(defn valid-message-form? [message]
  (nil? (first (validate-message-form message))))

(defn send-message-page [db channel-id request & [[errors old-form]]]
  (let [user-id (:identity request)
        user (get-in request [:session :user])
        channel (channel-model/by-channel-id db channel-id)]
    (if-not (= user-id (:account_id channel))
      (buddy-auth/throw-unauthorized {:message "Current user should be owner of channel"})
      (message-view/send-message-page user channel old-form errors))))

(defn send-message [db input-chan channel-id {{:strs [title text link]} :form-params :as request}]
  (let [message {:title title
                 :text text
                 :link link}]
    (if (valid-message-form? message)
      (let [channel (channel-model/by-channel-id db channel-id)
            chat-ids (tg-chat-model/by-channel-id db channel-id)
            channel-message {:message message
                             :type :channel-message
                             :chat-ids (set (map :chat_id chat-ids))
                             :channel-id channel-id}]
        (put-notification-worker-message! input-chan channel-message)
        (response/redirect (str "/channel/" channel-id)))
      (send-message-page db channel-id request (validate-message-form message)))))
