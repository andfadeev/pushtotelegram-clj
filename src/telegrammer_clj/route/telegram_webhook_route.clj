(ns telegrammer-clj.route.telegram-webhook-route
  (:require [clojure.walk :as walk]
            [ring.util.response :as ring-util]
            [schema.core :as s]
            [telegrammer-clj.schema.message-schema :as message-schema]
            [clojure.core.async :as async]))

(defn telegram-webhook [db channel request]
  (println "in webhook")
  (let [telegram-update-body (walk/keywordize-keys (:body request))
        chat-id (get-in telegram-update-body [:message :chat :id])
        action (get-in telegram-update-body [:message :text])]
    (async/put! channel {:action action
                         :chat-id chat-id}))
  (ring-util/response "OK"))

