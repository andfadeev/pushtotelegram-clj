(ns telegrammer-clj.route.auth.google-route
  (:require [cheshire.core :as json]
            [clj-http.client :as client]
            [clojure.walk :refer [keywordize-keys]]
            [compojure.core :refer [defroutes GET]]
            [ring.util
             [codec :as url]
             [response :as response]]
            [telegrammer-clj.auth :as auth]
            [telegrammer-clj.model.account-model :as account-model]
            [environ.core :refer [env]]))

(def GOOGLE_CLIENT_ID (env :google-client-id))
(def GOOGLE_CLIENT_SECRET (env :google-client-secret))
(def GOOGLE_REDIRECT_URL (str "http://" (env :host-name) "/auth/google_callback"))

(defn get-google-access-token [code]
  (-> "https://accounts.google.com/o/oauth2/token"
      (client/post {:form-params {:code code
                                  :client_id GOOGLE_CLIENT_ID
                                  :client_secret GOOGLE_CLIENT_SECRET
                                  :redirect_uri GOOGLE_REDIRECT_URL
                                  :grant_type "authorization_code"}})
      :body
      json/parse-string
      (get "access_token")))

(defn get-google-account [token]
  (-> "https://www.googleapis.com/oauth2/v1/userinfo?access_token=%s"
      (format token)
      client/get
      :body
      json/parse-string
      keywordize-keys))

(defn google-callback [db {{:strs [code state]} :query-params :as request}]
  (let [token (get-google-access-token code)
        google-account (get-google-account token)
        next-url (if state (url/url-decode state) "/")]
    (if-let [found-account (account-model/by-google-id db (:id google-account))]
      (auth/auth-session found-account next-url)
      (auth/auth-session (account-model/insert! db {:google_id (:id google-account)
                                                 :name (:name google-account)
                                                 :image_url (:picture google-account)
                                                 :email (:email google-account)}) next-url))))

(defn google-login-dialog-url [next]
  (str "https://accounts.google.com/o/oauth2/auth?scope=email%20profile"
       "&redirect_uri=" GOOGLE_REDIRECT_URL
       "&state=" (url/url-encode next)
       "&response_type=code&client_id=" GOOGLE_CLIENT_ID
       "&approval_prompt=auto"))

(defn auth-google [next request]
  (response/redirect (google-login-dialog-url (or next "/"))))

