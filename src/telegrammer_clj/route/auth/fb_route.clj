(ns telegrammer-clj.route.auth.fb-route
  (:require [cheshire.core :as json]
            [clj-http.client :as client]
            [clojure.walk :refer [keywordize-keys]]
            [compojure.core :refer [defroutes GET]]
            [ring.util
             [codec :as url]
             [response :as response]]
            [telegrammer-clj.auth :as auth]
            [telegrammer-clj.model.account-model :as account-model]
            [environ.core :refer [env]]))

(def FB_CLIENT_ID (env :fb-client-id))
(def FB_CLIENT_SECRET (env :fb-client-secret))
(def FB_REDIRECT_URL (str "http://" (env :host-name) "/auth/fb_callback"))

(defn get-fb-access-token [code]
  (-> "https://graph.facebook.com/v2.3/oauth/access_token?scope=email&client_id=%s&redirect_uri=%s&client_secret=%s&code=%s"
      (format FB_CLIENT_ID FB_REDIRECT_URL FB_CLIENT_SECRET code)
      client/get
      :body
      json/parse-string
      (get "access_token")))

(defn get-fb-account [token]
  (-> "https://graph.facebook.com/me?scope=email&access_token=%s"
      (format token)
      client/get
      :body
      json/parse-string
      keywordize-keys))

(defn fb-callback [db {{:strs [code state]} :query-params :as request}]
  (let [token (get-fb-access-token code)
        fb-account (get-fb-account token)
        next-url (url/url-decode (or state "/"))]
    (if-let [found-account (account-model/by-facebook-id db (:id fb-account))]
      (auth/auth-session found-account next-url)
      (let [new-account {:fb_id (:id fb-account)
                         :name (:name fb-account)
                         :email (:email fb-account)
                         :image_url (format "http://graph.facebook.com/%s/picture?type=large" (:id fb-account))}
            account (account-model/insert! db new-account)]
        (auth/auth-session account next-url)))))

(defn auth-fb [next request]
  (response/redirect
   (format "https://www.facebook.com/dialog/oauth?client_id=%s&state=%s&scope=email&redirect_uri=%s"
           FB_CLIENT_ID (or next "/") FB_REDIRECT_URL)))

(defroutes fb-routes
  (GET "/fb" [next :as request] (auth-fb next request))
  (GET "/fb_callback" [] fb-callback))
