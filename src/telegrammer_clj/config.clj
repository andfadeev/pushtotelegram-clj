(ns telegrammer-clj.config
  (:require [environ.core :refer [env]]))

(def defaults
  ^:displace {:http {:port 3000}})

(def environ
  {:http {:port (some-> env :port Integer.)
          :ip (env :host)}
   :db   {:uri (env :db-url)
          :username (env :db-username)
          :password (env :db-password)}})
