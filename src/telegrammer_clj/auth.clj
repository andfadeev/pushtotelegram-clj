(ns telegrammer-clj.auth
  (:require [buddy.auth.accessrules :refer [error success]]
            [buddy.auth :as b]
            [compojure.core :refer [defroutes GET]]
            [ring.util.response :as response :refer [redirect]]
            [telegrammer-clj.view.layout :as layout]
            [telegrammer-clj.view.nav :as menu]))

(defn auth-session [account next-url]
  (assoc (redirect next-url)
         :session
         {:identity (:account_id account) :user account}))

(defn logout [request]
  (assoc (redirect "/") :session {}))

(defn login-page [next request]
  (let [user (get-in request [:session :user])]
    (println user)
    (layout/with-container
      (menu/nav :login-page user)
      (if-not user
        [:div.row
         [:div.col.s12 {:style "text-align: center;"}
          [:h3 "Login"]
          [:p "Your can login with your social network account"]
          ;; [:a.btn.waves-effect.waves-light {:href (str "/auth/vk" (when next (str "?next=" next)))
          ;;                                   :style "background-color: #537BAB; margin: 5px;"}
          ;;  [:i.fa.fa-vk.left] "Vkontakte"]
          [:a.btn.waves-effect.waves-light {:href (str "/auth/fb"
                                                       (when next (str "?next=" next)))
                                            :style "background-color: #3b5998; margin: 5px;"}
           [:i.fa.fa-facebook-official.left] "Facebook"]
          [:a.btn.waves-effect.waves-light {:href (str "/auth/google"
                                                       (when next (str "?next=" next)))
                                            :style "background-color: #db4a39; margin: 5px;"}
           [:i.fa.fa-google-plus-square.left] "Google+"]]]
        [:div.row
         [:div.col.s12
          [:h4 "You are currently logged in, want to log out?"]
          [:p [:a {:href "/logout"} "Logout"]]]]))))

(defn user-access
  [request]
  (if (b/authenticated? request)
    (success)
    (error "Only authenticated users allowed")))

(def rules [{:uris ["/new-channel" "/my-channels" "/my-profile"]
             :handler user-access}
            {:pattern #"^/link-chat/.*"
             :handler user-access}
            {:pattern #"^/send-message/.*"
             :handler user-access}
            {:pattern #"^/channel/.*/subscribe"
             :handler user-access}
            {:pattern #"^/channel/.*/unsubscribe"
             :handler user-access}])

(defn on-error
  [request value]
  (println "on-error")
  (let [current-url (:uri request)]
    (response/redirect (format "/login?next=%s" current-url))))



