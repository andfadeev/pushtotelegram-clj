(ns telegrammer-clj.model.tg-chat-model
  (:require [clojure.java.jdbc :as j]
            [honeysql.core :as sql]
            [honeysql.helpers :refer :all]
            [telegrammer-clj.sql.helpers :as h]))

(defn by-chat-id [db chat-id]
  (first (j/query db
                  (h/by-id :tg_chat :chat_id chat-id))))

(defn by-channel-id [db channel-id]
  "Get telegram chat ids of channel subscribers"
  (j/query db (-> (select :chat_id)
                  (from [:tg_chat :tc])
                  (merge-left-join [:account :a] [:= :a.account_id :tc.account_id])
                  (merge-left-join [:subscriber :s] [:= :s.account_id :a.account_id])
                  (where [:= :s.channel_id channel-id])
                  (sql/format))))

(defn all [db]
  (j/query db (h/all :tg_chat)))

(defn exists? [db chat-id account-id]
  (-> (j/query db (-> (select :*)
                      (from :tg_chat)
                      (where [:= :chat_id chat-id]
                             [:= :account_id account-id])
                      (sql/format)))
      seq
      boolean))

(defn create! [db chat-id account-id]
  (first (j/query db (-> (insert-into :tg_chat)
                         (values [{:account_id account-id
                                   :chat_id chat-id}])
                         (h/returning :tg_chat_id)
                         (sql/format)))))
