(ns telegrammer-clj.model.channel-model
  (:require [clj-time
             [coerce :refer [to-timestamp]]
             [local :refer [local-now]]]
            [clojure.string :as str]
            [clojure.java.jdbc :as j]
            [honeysql.core :as sql]
            [honeysql.helpers :refer :all]
            [schema.core :as s]
            [telegrammer-clj.sql.helpers :as h]))

(defn- generate-channel-api-key []
  (str/join (filter (fn [char] (not= \- char)) (str (java.util.UUID/randomUUID)))))

(defn only-public [db]
  (j/query db (-> (select :*)
                  (from :channel)
                  (where [:= :is_private false])
                  (sql/format))))

(defn by-channel-id [db channel-id]
  (println "in by channel id " db)
  (first (j/query db (h/by-id :channel :channel_id channel-id))))

(defn by-owner-id [db owner-id]
  (j/query db (h/by-id :channel :account_id owner-id)))

(defn by-subscriber-id [db subscriber-id]
  (j/query db (-> (select :*)
                  (from [:channel :c])
                  (left-join [:subscriber :s] [:= :s.channel_id :c.channel_id])
                  (where [:= :s.subscriber_id subscriber-id])
                  (sql/format))))

(defn by-api-key [db api-key]
  (first (j/query db (-> (select :*)
                         (from :channel)
                         (where [:= :api_key api-key])
                         (sql/format)))))

(defn create-channel [db channel]
  (let [channel (assoc channel
                       :create_date (to-timestamp (local-now))
                       :api_key (generate-channel-api-key))]
    (:channel_id (first (j/query db (-> (insert-into :channel)
                                        (values [channel])
                                        (h/returning :channel_id)
                                        (sql/format)))))))




