(ns telegrammer-clj.model.subscriber-model
  (:require [clojure.java.jdbc :as j]
            [honeysql.helpers :refer :all]
            [honeysql.core :as sql]
            [telegrammer-clj.sql.helpers :as h]))

(defn subscribe [db account-id channel-id]
  (first (j/query db (-> (insert-into :subscriber)
                         (values [{:account_id account-id
                                   :channel_id channel-id}])
                         (h/returning :subscriber_id)
                         (sql/format)))))

(defn unsubscribe! [db account-id channel-id]
  (j/execute! db (-> (delete-from :subscriber)
                  (where [:= :account_id account-id]
                         [:= :channel_id channel-id])
                  (sql/format))))

(defn subscribed? [db channel-id account-id]
  (boolean (seq (j/query db (-> (select :subscriber_id)
                                (from :subscriber)
                                (where [:= :account_id account-id]
                                       [:= :channel_id channel-id])
                                (sql/format))))))

(defn by-channel-id [db channel-id]
  (j/query db (-> (select :*)
                  (from :subscriber)
                  (where [:= :channel_id channel-id])
                  (sql/format))))







