(ns telegrammer-clj.model.account-model
  (:require [clj-time
             [coerce :refer [to-timestamp]]
             [local :refer [local-now]]]
            [clojure.java.jdbc :as j]
            [honeysql.core :as sql]
            [honeysql.helpers :refer :all]
            [korma.core :as k]
            [telegrammer-clj.sql.helpers :as h]))

(defn by-facebook-id [db facebook-id]
  (first (j/query db (h/by-id :account :fb_id facebook-id))))

(defn by-google-id [db google-id]
  (first (j/query db (h/by-id :account :google_id google-id))))

(defn subscribers [db channel-id]
  (mapv #(assoc (dissoc (first %) :chat_id)
                :chats (mapv (fn [x] (select-keys x [:chat_id])) %))
        (partition-by
         :account_id
         (j/query db (-> (select :a.account_id, :a.name, :a.image_url :tc.chat_id)
                         (from [:subscriber :s])
                         (merge-left-join [:account :a] [:= :s.account_id :a.account_id])
                         (merge-left-join [:tg_chat :tc] [:= :a.account_id :tc.account_id])
                         (where [:= :s.channel_id channel-id])
                         (sql/format))))))

(defn insert! [db account]
  (let [account (assoc account :reg_date (to-timestamp (local-now)))
        account-id (:account_id (first (j/query db (-> (insert-into :account)
                                                       (values [account])
                                                       (h/returning :account_id)
                                                       (sql/format)))))]
    (assoc account :account_id account-id)))

