(ns telegrammer-clj.model.message-model
  (:require [clojure.java.jdbc :as j]
            [honeysql.core :as sql]
            [honeysql.helpers :refer :all]
            [clj-time
             [coerce :refer [to-timestamp]]
             [local :refer [local-now]]]
            [telegrammer-clj.sql.helpers :as h]))

(defn by-channel-id [db channel-id]
  (j/query db (-> (select :*)
                  (from :message)
                  (where [:= :channel_id channel-id])
                  (order-by [:create_date :desc])
                  (sql/format))))

(defn all [db]
  (j/query db (h/all :message)))

(defn save! [db message]
  (let [message (assoc (:message message)
                       :channel_id (:channel-id message)
                       :type "channel-message"
                       :create_date (to-timestamp (local-now)))]
    (j/query db (-> (insert-into :message)
                    (values [message])
                    (h/returning :message_id)
                    (sql/format)))))


