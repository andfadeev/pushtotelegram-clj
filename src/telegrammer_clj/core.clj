(ns telegrammer-clj.core
  (:require [meta-merge.core :refer [meta-merge]]
            [com.stuartsierra.component :as component]
            [telegrammer-clj
             [config :as config]
             [system :as system]]
            [telegrammer-clj.util.runtime :as runtime])
  (:gen-class))

(def config
  (meta-merge config/defaults
              config/environ))

(defn -main [& args]
  (println "starting system with config" config)
  (let [system (system/build-system config)]
    (runtime/add-shutdown-hook ::stop-system #(component/stop system))
    (component/start system)))

