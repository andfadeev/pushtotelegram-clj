(ns telegrammer-clj.migrations
  (:require [migratus.core :as migratus]
            [telegrammer-clj.state.db :as db]
            [clojure.tools.logging :as log]))

(def config {:store :database
             :migration-table-name "tg_schema_migration"
             :migration-dir "migrations/schema"
             :db db/dbspec})

(defn create-schema []
  (migratus/migrate config)
  (migratus/rollback config))

(defn migrate []
  (log/info "Migrating with dbspec" db/dbspec)
  (migratus/migrate config))

(defn rollback []
  (migratus/rollback config))
