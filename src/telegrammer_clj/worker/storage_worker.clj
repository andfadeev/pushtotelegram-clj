(ns telegrammer-clj.worker.storage-worker
  (:require [schema.core :as s]
            [telegrammer-clj.schema.message-schema :refer [ChannelMessage]]
            [telegrammer-clj.model.message-model :as message-model]))

(s/defn ^:always-validate storage-worker-fn
  [{{db :spec} :db} message :- ChannelMessage]
  (println "Storing channel message" message db)
  (message-model/save! db message))
