(ns telegrammer-clj.worker.telegram-bot-worker
  (:require [clj-http.client :as client]
            [environ.core :refer [env]]
            [ring.util.codec :as url]))

(defn send-telegram-message [chat-id text]
  (client/get (str "https://api.telegram.org/bot"
                   (env :telegram-token) "/sendMessage?chat_id=" chat-id
                   "&text=" text "&parse_mode=Markdown")))

(defmulti telegram-bot-worker-fn (fn [component message] (:action message)))

(defmethod telegram-bot-worker-fn
  "/start"
  [_ {:keys [action chat-id]}]
  (let [text (url/url-encode (format "*Hi! I'm PushToTelegramBot, nice to see you!*

I will deliver messages from PushToTelegram service.

Ready to start? Follow link: %s

Use /help command for more info.

If you have any questions, please contact @andfadeev"
                                     (str "http://www." (env :host-name) "/link-chat/" chat-id)))]
    (send-telegram-message chat-id text)))

(defmethod telegram-bot-worker-fn
  :default
  [_ {:keys [action chat-id]}]
  (let [text (url/url-encode (format "*Available PushToTelegramBot commands:*

/start - link your telegram with PushToTelegram account
/help - help info about bot

More bot commands comming soon...

If you have any questions, please contact @andfadeev
%s" (str "http://www." (env :host-name))))]
    (send-telegram-message chat-id text)))
