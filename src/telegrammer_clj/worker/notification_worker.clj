(ns telegrammer-clj.worker.notification-worker
  (:require [clj-http.client :as client]
            [clojure.tools.logging :as log]
            [environ.core :refer [env]]
            [schema.core :as s]
            [telegrammer-clj.schema.message-schema
             :refer
             [ChannelMessage put-storage-worker-message!]]))

(def send-message-url (format "https://api.telegram.org/bot%s/sendMessage?" (env :telegram-token)))

(defmulti notification-worker-fn (fn [component message] (:type message)))

(s/defmethod ^:always-validate notification-worker-fn
  :channel-message
  [{storage-channel :result-chan} message :- ChannelMessage]
  (println "Handling channel message" message)
  (let [title (get-in message [:message :title])
        text (get-in message [:message :text])
        link (get-in message [:message :link])
        message-text (str title "\n\n" text "\n\n" link)
        chat-id (:chat-id message)]
    (client/get (str send-message-url "chat_id=" chat-id "&text=" message-text)))
  (put-storage-worker-message! storage-channel message))

