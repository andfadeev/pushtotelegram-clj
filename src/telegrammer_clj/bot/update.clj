(ns telegrammer-clj.bot.update
  (:require [cheshire.core :as json]
            [clj-http.client :as client]
            [clojure.core.async :as async]
            [clojure.tools.logging :as log]
            [mount.core :as mount]
            [telegrammer-clj.bot.message-listener :as message-listener]
            [telegrammer-clj.model.tg-chat-model :as tg-chat-model]
            [environ.core :as e]))

  ;; 170876663:AAEXUIvzv26Y6AXA913m08-mLD5iV5_bSeQ
(def get-updates-url (format "https://api.telegram.org/bot%s/getUpdates" (e/env :telegram-token)))

(def offset (atom nil))

(defn get-updates []
  (let [url (if @offset
              (str get-updates-url "?offset=" @offset)
              get-updates-url)
        updates (-> url
                    client/get
                    :body
                    (json/parse-string true)
                    :result)]
    (when (seq updates)
      (println updates)
      (doseq [update updates]
        (let [chat-id (get-in update [:message :chat :id])
              tg-chat (tg-chat-model/by-chat-id chat-id)
              _ (println tg-chat)
              _ (println chat-id)]
          (if tg-chat
            (println "implement more commands here")
            (message-listener/put-message! {:type :welcome-message
                                            :chat-id chat-id}))))
      (reset! offset (-> updates last :update_id inc)))))

(defn start-update-listener [kill-chan]
  (log/info "Start telegram update listener loop")
  (async/thread
    (async/>!! kill-chan :run)
    (while (async/<!! kill-chan)
      (try
        (get-updates)
        (async/<!! (async/timeout 3000))
        (async/>!! kill-chan :run)
        (catch Throwable t
          (log/error "Error in telegram update listener loop" t))))
    (log/info "Telegram update listener loop is stopped")))

(mount/defstate update-listener
  :start ((fn [] (let [kill-chan (async/chan 1)]
                  (start-update-listener kill-chan)
                  kill-chan)))
  :stop ((fn [kill-chan] (async/close! kill-chan)) update-listener))


