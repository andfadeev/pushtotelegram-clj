(ns telegrammer-clj.bot.db-message-channel
  (:require [clojure.core.async :as a]
            [clojure.tools.logging :as log]
            [mount.core :as mount]
            [schema.core :as s]
            [telegrammer-clj.schema.message-schema :refer [ChannelMessage]]
            [telegrammer-clj.model.message-model :as message-model]))

(s/defn ^:always-validate handle-db-message
  [message :- ChannelMessage]
  (log/info "Saving channel message to db" message)
  (let [title (get-in message [:message :title])
        text (get-in message [:message :text])
        link (get-in message [:message :link])]
    :nil))

(defn start-db-message-listener
  ([chan]
   (start-db-message-listener 1 chan))
  ([num chan]
   (dotimes [n num]
     (a/thread
       (log/info "Start db message listener" n)
       (loop []
         (when-let [message (a/<!! chan)]
           (try
             (handle-db-message message)
             (catch Throwable t
               (log/error "Error in db message listener" t)))
           (recur)))
       (log/info "Db message sender is stopped")))))

(defn create-db-message-channel []
  (let [channel (a/chan 100)]
    (start-db-message-listener channel)
    channel))

(defn close-db-message-channel [chan]
  (a/close! chan))

(mount/defstate db-message-channel
  :start (create-db-message-channel)
  :stop (close-db-message-channel db-message-channel))

(s/defn put-message! [message :- ChannelMessage]
  (a/put! db-message-channel message))



