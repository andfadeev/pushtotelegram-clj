(ns telegrammer-clj.bot.message-listener
  (:require [clojure.core.async :as a]
            [mount.core :as mount]
            [schema.core :as s]
            [telegrammer-clj.schema.message-schema :refer [Message]]))

(defn create-message-listener-channel []
  (a/chan 100))

(defn close-message-listener-channel [channel]
  (a/close! channel))

(mount/defstate message-listener-channel
  :start (create-message-listener-channel)
  :stop (close-message-listener-channel message-listener-channel))

(s/defn put-message! [message :- Message]
  (a/put! message-listener-channel message))
