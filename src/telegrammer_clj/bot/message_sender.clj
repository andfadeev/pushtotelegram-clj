(ns telegrammer-clj.bot.message-sender
  (:require [clj-http.client :as client]
            [clojure.core.async :as a]
            [clojure.tools.logging :as log]
            [ring.util.codec :as url]
            [schema.core :as s]
            [telegrammer-clj.bot.db-message-channel :as db-message-channel]
            [telegrammer-clj.schema.message-schema
             :refer
             [ChannelMessage WelcomeMessage]]
            [environ.core :as e]))

(def send-message-url (format "https://api.telegram.org/bot%s/sendMessage?" (e/env :telegram-token)))

(defmulti handle-message :type)

(s/defmethod ^:always-validate handle-message
  :welcome-message
  [message :- WelcomeMessage]
  (log/info "Handling welcome message" message)
  (let [chat-id (:chat-id message)
        link-chat-url (str (e/env :site-url) "link-chat/" chat-id)
        message-text (url/url-encode (format "*Hi! I'm PushToTelegramBot, nice to see you!*

I will deliver messages from PushToTelegram service.

Ready to start? Follow link: %s

If you have any questions, please contact @andfadeev" link-chat-url))
        send-message-url (str send-message-url "chat_id=" chat-id "&text=" message-text "&parse_mode=Markdown")]
    (client/get send-message-url)))

(s/defmethod ^:always-validate handle-message
  :channel-message
  [message :- ChannelMessage]
  (log/info "Handling channel message" message)
  (when-not (empty? (:chat-ids message))
    (let [title (get-in message [:message :title])
          text (get-in message [:message :text])
          link (get-in message [:message :link])
          message-text (str title "\n\n" text "\n\n" link)
          chat-ids (:chat-ids message)]
      (doseq [chat-id chat-ids]
        (client/get (str send-message-url "chat_id=" chat-id "&text=" message-text)))))
  (db-message-channel/put-message! message))

(defn start-notif-handlers
  ([chan]
   (start-notif-handlers 1 chan))
  ([num chan]
   (dotimes [n num]
     (a/thread
       (log/info "Start message sender" n)
       (loop []
         (when-let [message (a/<!! chan)]
           (try
             (handle-message message)
             (catch Throwable t
               (log/error "Error in message sender" t)))
           (recur)))
       (log/info "Message sender is stopped")))))
