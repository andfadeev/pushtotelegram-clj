(ns telegrammer-clj.view.nav)

(defn login-menu-item
  [active-item]
  (list
   [:ul#login-dropdown.dropdown-content
    [:li {:style "text-align: center;"} [:a {:href "/login"} "Log in"]]
    ;; [:li {:style "text-align: center;"} [:a {:href "/auth/vk"
    ;;                                          :style "color: #537BAB;"} [:i.fa.fa-vk]]]
    [:li {:style "text-align: center;"} [:a {:href "/auth/google"
                                             :style "color: #db4a39"} [:i.fa.fa-google-plus-square]]]
    [:li {:style "text-align: center;"} [:a {:href "/auth/fb"
                                             :style "color: #3b5998"} [:i.fa.fa-facebook-official]]]]
   [:li (when (= active-item :comments) {:class "active"})
    [:a.dropdown-button {:href "#!" :data-activates "login-dropdown"} "Log in" [:i.material-icons.right "arrow_drop_down"]]]))

(defn profile-menu-item
  [active-item user]
  (list
   [:ul#profile-dropdown.dropdown-content
    [:li [:a {:href "/my-channels"} "My channels"]]
    [:li [:a {:href "/logout"} "Logout"]]]
   [:li (when (= active-item :comments) {:class "active"})
    [:a.dropdown-button {:href "#!" :data-activates "profile-dropdown"}
     [:img.circle.left {:src (:image_url user)
                        :style "height: 44px; margin-top: 9px; margin-right: 5px;"}]
     [:span (:name user)]
     [:i.material-icons.right "arrow_drop_down"]]]))

(defn top-menu [user active-item]
  [:ul.right.hide-on-med-and-down
   [:li (when (= active-item :channels) {:class "active"})
    [:a {:href "/channels"} "Channels"]]
   [:li (when (= active-item :docs) {:class "active"})
    [:a {:href "/docs"} "Docs"]]
   (if user (profile-menu-item active-item user) (login-menu-item active-item))])

(defn mobile-nav [user]
  [:ul#mobile-nav.side-nav {:style "left: -250px;"}
   [:li.logo {:style "margin-bottom: 110px; margin-top: 10px;"}
    [:a.brand-logo {:href "/"}
     [:img.responsive-img.circle {:src (or (:image_url user) "/default-channel-img.png")}]]]
   [:li [:a {:href "/channels"} "Channels"]]
   [:li [:a {:href "/docs"} "Docs"]]
   (if user
     (list [:li [:a {:href "/my-channels"} "My channels"]]
           [:li [:a {:href "/logout"} "Logout"]])
     [:li.no-padding
      [:ul.collapsible.collapsible-accordion
       [:li.bold
        [:a.collapsible-header "Login"]
        [:div.collapsible-body {:style "display: block;"}
         [:ul
          [:li [:a {:href "/auth/google"} "Google+"]]
          [:li [:a {:href "/auth/fb"} "Facebook"]]
          ;;[:li [:a {:href "/auth/vk"} "Vkontakte"]]
          ]]]]])])

(defn nav
  ([user] (nav :no-active user))
  ([active-item user]
   [:nav.light-blue.lighten-1 {:role "navigation"}
    [:div.nav-wrapper.container
     [:a#logo-container.brand-logo {:href "/"} "PushToTelegram"]
     [:a.button-collapse {:href "#" :data-activates "mobile-nav"} [:i.material-icons "menu"]]
     (top-menu user active-item)
     (mobile-nav user)]]))
