(ns telegrammer-clj.view.channel-view
  (:require [ring.util.anti-forgery :refer [anti-forgery-field]]
            [telegrammer-clj.view
             [layout :as l]
             [nav :as nav]
             [message-view :as message-view]]))

(defn channel-card [channel]
  [:div.card-panel.lighten-5.z-depth-1 {:style "word-wrap: break-word;"}
   [:div.row.valign-wrapper {:style "margin-bottom: 0px;"}
    [:div.col.s2
     [:img.responsive-img {:src (:image_url channel)}]]
    [:div.col.s10
     [:b [:a.blue-text {:href (str "/channel/" (:channel_id channel))} (:name channel)]]
     [:p (:info channel)]]]])

(defn subscriber-card [sub]
  [:div.card-panel.lighten-5.z-depth-1
   [:div.row.valign-wrapper {:style "margin-bottom: 0px;"}
    [:div.col.s4
     [:img.circle.responsive-img {:src (:image_url sub)}]]
    [:div.col.s8
     [:b (:name sub)
      (when-let [chats (seq (:chats sub))]
        (list
         [:br]
         (for [chat chats]
           [:img.tooltipped {:src "https://raw.githubusercontent.com/telegramdesktop/tdesktop/master/Telegram/Telegram/Images.xcassets/Icon.iconset/icon_256x256@2x.png"
                             :width "24px;"
                             :data-position "left"
                             :data-delay "50"
                             :data-tooltip (str "Telegram Chat: " (:chat_id chat))}])))]]]])

(defn channels-two-col-block
  [block-title channels]
  (list
   [:div.row [:div.col.s12 [:h3 block-title]]]
   [:div.row
    [:div.col.s12.l6
     (for [channel (take-nth 2 channels)]
       [:div.col.s12 (channel-card channel)])]
    [:div.col.s12.l6
     (for [channel (take-nth 2 (rest channels))]
       [:div.col.s12 (channel-card channel)])]]))

(defn create-channel-form [& [[errors old-form]]]
  (println errors)
  (list
   [:div.row [:div.col.s12 [:h3 "Create new channel"]]]
   [:div.row
    [:div.col.s12.l6
     [:form {:method "post" :action "/create-channel" :enctype "multipart/form-data"}
      [:div.row
       [:div.file-field.input-field.col.s12
        [:div.btn
         [:span "Channel Image"]
         [:input#channel-image {:name "channel-image" :type "file"}]]
        [:div.file-path-wrapper
         [:input.file-path.validate {:type "text"
                                     :placeholder "Upload channel image"}]]
        [:div (when (get-in errors [:file :size]) [:small.red-text (get-in errors [:file :size])])]]]
      [:div.row
       [:div.input-field.col.s12
        [:input#channel-name {:type "text" :name "channel-name"
                              :value (:name old-form)}]
        [:label {:for "channel-name"} "Channel name"]
        (when (:name errors) [:small.red-text (:name errors)])]]
      [:div.row
       [:div.input-field.col.s12
        [:textarea#channel-info.materialize-textarea
         {:name "channel-info"} (:info old-form)]
        [:label {:for "channel-info"} "Channel description"]
        (when (:info errors) [:small.red-text (:info errors)])]]
      [:div.row
       [:div.input-field.col.s12
        [:input#channel-links {:type "text" :name "channel-links"
                               :value (:links old-form)}]
        [:label {:for "channel-links"} "Links (separated with comma)"]
        (when (:links errors) [:small.red-text (:links errors)])]]
      [:div.row
       [:div.col.s12
        [:input#private-channel.filled-in {:type "checkbox" :name "is-private"}]
        [:label {:for "private-channel"} "Private channel"]]]
      [:div.row
       [:div.col.s12
        [:button.btn.waves-effect.waves-light {:type "submit"} "Create channel"]
        (anti-forgery-field)]]]]]))

(defn new-channel-fixed-btn []
  [:div.fixed-action-btn {:style "bottom: 45px; right: 24px;"}
   [:a.btn-floating.btn-large.waves-effect.waves-light.red.tooltipped
    {:href "/new-channel"
     :data-position "left"
     :data-delay "50"
     :data-tooltip "Create new channel"}
    [:i.material-icons "add"]]])

(defn tooltipped-floating-btn [{:keys [class href icon tooltip]}]
  [:a.btn-floating.tooltipped
   {:href href
    :class class
    :data-position "left"
    :data-delay "50"
    :data-tooltip tooltip}
   [:i.material-icons icon]])

(defn new-message-fixed-btn [channel-id]
  [:div.fixed-action-btn {:style "bottom: 45px; right: 24px;"}
   [:a.btn-floating.btn-large.waves-effect.waves-light.red.tooltipped
    {:href (str "/send-message/" channel-id)
     :data-position "left"
     :data-delay "50"
     :data-tooltip "New message"}
    [:i.material-icons "mode_edit"]]])

(defn subscribe-fixed-btn [user subs channel-id]
  [:div.fixed-action-btn {:style "bottom: 45px; right: 24px;"}
   (if (some #(= (:account_id user) (:account_id %)) subs)
     (tooltipped-floating-btn {:class "red btn-large"
                               :href (str "/channel/" channel-id "/unsubscribe")
                               :icon "not_interested"
                               :tooltip "Unsubscribe"})
     (tooltipped-floating-btn {:class "tile btn-large"
                               :href (str "/channel/" channel-id "/subscribe")
                               :icon "done"
                               :tooltip "Subscribe to channel"}))])

(defn channel-page-owner-fab [user channel-id subs]
  [:div.fixed-action-btn.click-to-toggle {:style "bottom: 45px; right: 24px;"}
   [:a.btn-floating.btn-large.red
    [:i.large.mdi-navigation-menu]]
   [:ul
    (if (some #(= (:account_id user) (:account_id %)) subs)
      [:li (tooltipped-floating-btn {:class "red"
                                     :href (str "/channel/" channel-id "/unsubscribe")
                                     :icon "not_interested"
                                     :tooltip "Unsubscribe"})]
      [:li (tooltipped-floating-btn {:class "tile"
                                     :href (str "/channel/" channel-id "/subscribe")
                                     :icon "done"
                                     :tooltip "Subscribe to channel"})])
    [:li (tooltipped-floating-btn {:class "blue"
                                   :href (str "/send-message/" channel-id)
                                   :icon "email"
                                   :tooltip "Send message"})]
    ;; [:li (tooltipped-floating-btn {:class "orange"
    ;;                                :href "/login"
    ;;                                :icon "publish"
    ;;                                :tooltip "Send message"})]
    ]])

(defn channel-page [channel links subscribers messages user]
  (l/with-container
    (nav/nav user)
    (if-not (nil? channel)
      (if-not (and (:is_private channel) (not= (:account_id user) (:account_id channel)))
        (list
         [:div.row
          [:div.col.s12
           [:h4 (:name channel)]
           [:p (:info channel)]
           (when (seq links)
             [:ul (for [link links]
                    [:li [:b [:a {:href link
                                  :target "_blank"} link]]])])
           (when (= (:account_id channel) (:account_id user))
             (list [:ul.collapsible.collapsible-accordion {:data-collapsible "accordion"}
                    [:li
                     [:div.collapsible-header.active
                      [:i.material-icons "assignment_ind"] "Administration"]
                     [:div.collapsible-body [:p [:b "Channel api key: "] (:api_key channel)]]]]))]]
         [:div.row
          [:div.col.s12.m7
           (if (empty? messages)
             [:h5 "No messages"]
             (list
              [:h5 (format "Messages (%s)" (count messages))]
              [:div.row
               (for [message messages]
                 [:div.col.s12
                  (message-view/message-card message (:image_url channel))])]))]
          [:div.col.s12.m5
           (if (empty? subscribers)
             [:h5 "No subscribers"]
             (list
              [:h5 (format "Subscribers (%s)" (count subscribers))]
              [:div.row
               (for [sub subscribers]
                 [:div.col.s12
                  (subscriber-card sub)])]))]]
         (if (= (:account_id channel) (:account_id user))
           (channel-page-owner-fab user (:channel_id channel) subscribers)
           (subscribe-fixed-btn user subscribers (:channel_id channel))))
        [:div.row
         [:div.col.s12
          [:h4 "Sorry, this channel is private"]
          [:p [:a {:href "/"} "Go to main page"]]]])
      [:div.row
       [:div.col.s12
        [:h4 "Sorry, channel doesn't exist"]
        [:p [:a {:href "/"} "Go to main page"]]]])
    {:title (str "PushToTelegram - " (:name channel))
     :description (:info channel)}))

(defn channels-page [user channels]
  (l/with-container (nav/nav :channels user)
    (list
     (channels-two-col-block "Channels" channels)
     (new-channel-fixed-btn))
    {:title "PustToTelegram - Channels"
     :description "List of all available channels on PushToTelegram website."}))

(defn my-channels-page [user owned-channels subscribed-channels]
  (l/with-container (nav/nav :channels user)
    (list
     (channels-two-col-block "Owned channels" owned-channels)
     (channels-two-col-block "Subscribed channels" subscribed-channels)
     (new-channel-fixed-btn))))
