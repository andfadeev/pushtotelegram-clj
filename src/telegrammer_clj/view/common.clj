(ns telegrammer-clj.view.common)

(defn textarea [id name label val error]
  [:div.row
   [:div.input-field.col.s12
    [:textarea.materialize-textarea
     {:id id :name name} val]
    [:label {:for id} label]
    (when error [:small.red-text error])]])

(defn text-input [{:keys [id name label value error]}]
  [:div.row
   [:div.input-field.col.s12
    [:input {:id id :type "text" :name name :value value}]
    [:label {:for id} label]
    (when error [:small.red-text error])]])


