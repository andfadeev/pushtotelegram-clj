(ns telegrammer-clj.view.message-view
  (:require [ring.util.anti-forgery :as f]
            [telegrammer-clj.view
             [common :as c]
             [layout :as view]
             [nav :as menu]]))

(defn message-card [message channel-img]
  [:div.card-panel.lighten-5.z-depth-1
   [:div.row.valign-wrapper {:style "margin-bottom: 0px;"}
    [:div.col.s2
     [:img.responsive-img {:src channel-img}]]
    [:div.col.s10
     [:b [:a.blue-text {:href (:link message)
                        :target "_blank"} (:title message)]]
     [:p (:text message)]]]])

(defn send-message-page [user channel & [old-form errors]]
  (view/with-container
    (menu/nav user)
    (list
     [:div.row
      [:div.col.s12
       [:h4 (:name channel)]
       [:p (:info channel)]]]
     [:div.row
      [:div.col.s12.l6
       [:h3 "Send new message"]
       [:form {:method "post" :action (str "/send-message/" (:channel_id channel))}
        (c/text-input {:id "message-title"
                       :name "title"
                       :label "Title"
                       :value (:title old-form)
                       :error (:title errors)})
        (c/textarea "text-id" "text" "Message text"
                  (:text old-form) (:text errors))
        (c/text-input {:id "link-id"
                       :name "link"
                       :label "Link"
                       :value (:link old-form)
                       :error (:link errors)})
        [:div.row
         [:div.col.s12
          [:button.btn.waves-effect.waves-light {:type "submit"} "Send message"]
          (f/anti-forgery-field)]]]]])))
