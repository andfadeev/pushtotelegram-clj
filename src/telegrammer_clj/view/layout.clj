(ns telegrammer-clj.view.layout
  (:require [hiccup.page :refer :all]))

(defn- favicon-html []
  (list
   [:link {:rel "apple-touch-icon", :sizes "57x57", :href "/apple-touch-icon-57x57.png"}]
   [:link {:rel "apple-touch-icon", :sizes "60x60", :href "/apple-touch-icon-60x60.png"}]
   [:link {:rel "apple-touch-icon", :sizes "72x72", :href "/apple-touch-icon-72x72.png"}]
   [:link {:rel "apple-touch-icon", :sizes "76x76", :href "/apple-touch-icon-76x76.png"}]
   [:link {:rel "apple-touch-icon", :sizes "114x114", :href "/apple-touch-icon-114x114.png"}]
   [:link {:rel "apple-touch-icon", :sizes "120x120", :href "/apple-touch-icon-120x120.png"}]
   [:link {:rel "apple-touch-icon", :sizes "144x144", :href "/apple-touch-icon-144x144.png"}]
   [:link {:rel "apple-touch-icon", :sizes "152x152", :href "/apple-touch-icon-152x152.png"}]
   [:link {:rel "apple-touch-icon", :sizes "180x180", :href "/apple-touch-icon-180x180.png"}]
   [:link {:rel "icon", :type "image/png", :href "/favicon-32x32.png", :sizes "32x32"}]
   [:link {:rel "icon", :type "image/png", :href "/android-chrome-192x192.png", :sizes "192x192"}]
   [:link {:rel "icon", :type "image/png", :href "/favicon-96x96.png", :sizes "96x96"}]
   [:link {:rel "icon", :type "image/png", :href "/favicon-16x16.png", :sizes "16x16"}]
   [:link {:rel "manifest", :href "/manifest.json"}]
   [:link {:rel "mask-icon", :href "/safari-pinned-tab.svg", :color "#5bbad5"}]
   [:meta {:name "msapplication-TileColor", :content "#da532c"}]
   [:meta {:name "msapplication-TileImage", :content "/mstile-144x144.png"}]
   [:meta {:name "theme-color", :content "#ffffff"}]))

(defn base-layout [nav body & [{:keys [title description]}]]
  (html5
   [:head
    (favicon-html)
    [:meta {:name "description" :content (or description "A simple service to notify your users with Telegram messenger")}]
    [:meta {:http-equiv "Content-Type"
            :content "text/html; charset=UTF-8"}]
    [:title (or title "PushToTelegram")]
    [:link {:href "https://fonts.googleapis.com/icon?family=Material+Icons"
            :rel "stylesheet"}]
    [:link {:href "/css/materialize.css"
            :type "text/css"
            :rel "stylesheet"
            :media "screen,projection"}]
    [:link {:href "/css/style.css"
            :rel "stylesheet"
            :media "screen,projection"}]
    [:link {:href "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"
            :rel "stylesheet"}]
    [:link {:href "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.1.0/styles/github.min.css"
            :rel "stylesheet"}]
    [:script {:src "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.1.0/highlight.min.js"}]]
   [:body nav body
    [:script {:src "https://code.jquery.com/jquery-2.1.1.min.js"}]
    [:script {:src "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"}]
    [:script {:src "/js/init.js"}]]))

(defn with-container
  [nav body & [meta]]
  (base-layout nav
   [:div#index-banner.section.no-pad-bot
    [:div.container body]]
   meta))
