(ns telegrammer-clj.service.channel-service
  (:require [clojure.java.jdbc :as j]
            [telegrammer-clj.model
             [channel-model :as channel-model]
             [subscriber-model :as subcriber-model]]))

(defn create-channel-new [db channel]
  (j/with-db-transaction [conn db]
    (let [channel-id (channel-model/create-channel conn channel)]
      (subcriber-model/subscribe conn (:account_id channel) channel-id)
      (assoc channel :channel_id channel-id))))
