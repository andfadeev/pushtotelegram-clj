(ns telegrammer-clj.state.db
  (:require [clojure.tools.logging :as log]
            [environ.core :as e]
            [korma.db :as k]
            [mount.core :as mount]))

(def dbspec {:subprotocol "postgresql"
             :subname (format "//%s:%s/%s" (e/env :db-host) (e/env :db-port) (e/env :db-name))
             :user (e/env :db-username)
             :password (e/env :db-password)})

(defn connect-korma! []
  (let [conn (k/create-db dbspec)]
    (log/info "Creating db connection with params" dbspec)
    (k/default-connection conn)
    (log/info "Db connection established")
    conn))

(mount/defstate ^:dynamic *korma-db*
  :start (connect-korma!))


