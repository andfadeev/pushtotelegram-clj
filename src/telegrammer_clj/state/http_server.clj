(ns telegrammer-clj.state.http-server
  (:require [buddy.auth
             [accessrules :refer [wrap-access-rules]]
             [middleware :as buddy-middleware]]
            [buddy.auth.backends.session :refer [session-backend]]
            [clojure.tools.logging :as log]
            [compojure
             [coercions :refer [as-int]]
             [core :refer [context defroutes GET POST routes]]
             [route :refer [resources]]]
            [environ.core :as e]
            [mount.core :as mount]
            [org.httpkit.server :as httpkit]
            [ring.middleware
             [defaults :as defaults]
             [json :refer [wrap-json-response
                           wrap-json-body]]
             [multipart-params :as mp]]
            [telegrammer-clj.auth :refer [on-error rules]]
            [telegrammer-clj.bot
             [message-listener :as message-listener]
             [message-sender :as message-sender]]
            [telegrammer-clj.model.channel-model :as channel-model]
            [telegrammer-clj.route
             [channel-route :as channel-route :refer [channel-page channels-page create-channel new-channel-page]]
             [doc-route :as doc-route]
             [message-route :as message-route]
             [telegram-webhook-route :as telegram-webhook-route]
             [tg-chat-route :as tg-chat-route]]
            [telegrammer-clj.route.auth
             [fb-route :as fb-routes]
             [google-route :as google-routes]]
            [telegrammer-clj.view
             [channel-view :refer [channels-two-col-block]]
             [layout :as view]
             [nav :refer [nav]]]))

;; (defroutes site
;;   (GET "/channels" [] channels-page)
;;   (GET "/my-channels" [] channel-route/my-channels-page)
;;   (GET "/channel/:id" [id :<< as-int :as request] (channel-page id request))
;;   (GET "/channel/:id/subscribe" [id :<< as-int :as request] (channel-route/subscribe id request))
;;   (GET "/channel/:id/unsubscribe" [id :<< as-int :as request] (channel-route/unsubscribe id request))
;;   (GET "/link-chat/:chat-id" [chat-id :<< as-int :as request]
;;     (tg-chat-route/link-account-to-chat chat-id request))
;;   (GET "/linked" [] tg-chat-route/linked-page)
;;   (GET "/new-channel" [] new-channel-page)
;;   (mp/wrap-multipart-params
;;    (POST "/create-channel" [channel-name
;;                             channel-info channel-links
;;                             is-private channel-image
;;                             :as request]
;;      (create-channel channel-name channel-info channel-links is-private channel-image request)))
;;   (GET "/send-message/:channel-id" [channel-id :<< as-int :as request]
;;     (message-route/send-message-page channel-id request))
;;   (POST "/send-message/:channel-id" [channel-id :<< as-int title text link :as request]
;;     (message-route/send-message channel-id title text link request))
;;   (resources "/"))


;; (defn start-http-server [config]
;;   (let [server (atom nil)]
;;     (message-sender/start-notif-handlers message-listener/message-listener-channel)
;;     (reset! server (httpkit/run-server #'all-routes {:ip (e/env :host)
;;                                                      :port (Integer/parseInt (e/env :port))}))
;;     (log/info "Http server has been started on port" (e/env :port))
;;     server))

;; (defn stop-http-server [server]
;;   (when-not (nil? @server)
;;     (@server :timeout 100)
;;     (reset! server nil))
;;   server)

;; (mount/defstate http-server
;;   :start (start-http-server nil)
;;   :stop (stop-http-server http-server))
