(ns telegrammer-clj.state.cloudinary
  (:require [clojure.tools.logging :as log]
            [environ.core :as e]
            [mount.core :as mount])
  (:import com.cloudinary.Cloudinary))

(mount/defstate cloudinary
  :start ((fn []
            (log/info "Creating connection to Cloudinary")
            (Cloudinary. {"cloud_name" (e/env :cloudinary-cloud-name)
                          "api_key" (e/env :cloudinary-api-key)
                          "api_secret" (e/env :cloudinary-api-secret)}))))

(defn get-uploader []
  (.uploader cloudinary))

;; {"cloud_name" "pushtotelegram"
;;  "api_key" "717865988872586"
;;  "api_secret" "r24blhYdKcKaMjv0XbcG4rbZhjY"}
