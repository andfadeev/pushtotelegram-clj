(ns telegrammer-clj.sql.helpers
  (:require [honeysql
             [core :as sql]
             [format :as f]
             [helpers :refer :all]]))

(defn by-id [table id-col id-val]
  (-> (select :*)
      (from table)
      (where [:= id-col id-val])
      (sql/format)))

(defn all [table]
  (-> (select :*)
      (from table)
      (sql/format)))

(defmethod f/format-clause :returning [[_ fields] _]
  (str "RETURNING " (f/comma-join (map f/to-sql fields))))

(f/register-clause! :returning 225)

(defhelper returning [m fields]
  (assoc m :returning (collify fields)))
