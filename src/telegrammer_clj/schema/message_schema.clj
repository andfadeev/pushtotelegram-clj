(ns telegrammer-clj.schema.message-schema
  (:require [schema.core :as s]
            [schema.experimental.abstract-map :as abstract-map]
            [clojure.core.async :as async]))

(s/defschema Message (abstract-map/abstract-map-schema :type {}))

(abstract-map/extend-schema WelcomeMessage Message [:welcome-message]
                            {:chat-id s/Num})

(abstract-map/extend-schema ChannelMessage Message [:channel-message]
                            {:message {:title s/Str
                                       :text s/Str
                                       (s/optional-key :link) (s/maybe s/Str)}
                             :channel-id s/Num
                             :chat-id s/Num})

(s/defn put-telegram-webhook-message! [channel message :- Message]
  (async/put! channel message))

(s/defn put-telegram-bot-message! [channel message :- Message]
  (async/put! channel message))

(s/defn put-notification-worker-message! [channel message :- Message]
  (async/put! channel message))

(s/defn put-storage-worker-message! [channel message :- Message]
  (async/put! channel message))

