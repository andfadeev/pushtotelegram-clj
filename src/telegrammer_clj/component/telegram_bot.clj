(ns telegrammer-clj.component.telegram-bot
  (:require [clj-http.client :as client]
            [clojure.core.async :as async]
            [com.stuartsierra.component :as component]
            [environ.core :refer [env]]))

(defrecord TelegramBot [name worker-fn]
  component/Lifecycle
  (start [component]
    (println "Starting" name)
    (let [stop-chan (async/chan 1)]
      (async/go-loop []
        (async/alt!
          (:in-channel component)
          ([msg]
           (try
             (worker-fn msg)
             (catch Throwable ignored
               (println "Error in TelegramBot" ignored)))
           (recur))
          stop-chan
          ([_]
           :no-op)))
      (assoc component :stop-chan stop-chan)))
  (stop [component]
    (println "Stopping telegram bot")
    (async/put! (:stop-chan component) :stop)
    (dissoc component :stop-chan)))

(defn telegram-bot [name worker-fn]
  (->TelegramBot worker-fn))

