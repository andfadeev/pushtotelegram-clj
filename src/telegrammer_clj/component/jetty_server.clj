(ns telegrammer-clj.component.jetty-server
  (:import org.eclipse.jetty.server.Server)
  (:require [com.stuartsierra.component :as component]
            [org.httpkit.server :as httpkit]
            [ring.adapter.jetty :as jetty]))

(defrecord JettyServer [options]
  component/Lifecycle
  (start [component]
    (println "Starting jetty server on port" (get-in component [:options :port]))
    (if (:server component)
      component
      (let [options (-> component :options (assoc :join? false))
            handler (atom (delay (:handler (:site-handler component))))
            ;;jetty-server (jetty/run-jetty (fn [req] (@@handler req)) options)
            http-kit-server (httpkit/run-server (fn [req] (@@handler req)) options)]
        (assoc component
               :handler handler
               :server http-kit-server))))
  (stop [component]
    (println "Stopping jetty server")
    (if-let [server (:server component)]
      ;; graceful shutdown: wait 100ms for existing requests to be finished
      ;; :timeout is optional, when no timeout, stop immediately
      (server :timeout 100)
      (dissoc component :server :handler))
    ;; (if-let [^Server server (:server component)]
    ;;   (do (.stop server)
    ;;       (.join server)
    ;;       (dissoc component :server :handler))
    ;;   component)
    ))

(defn jetty-server [options]
  "Create jetty server component with options"
  (map->JettyServer {:options options}))
