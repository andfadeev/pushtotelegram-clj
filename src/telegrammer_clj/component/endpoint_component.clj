(ns telegrammer-clj.component.endpoint-component
  (:require [com.stuartsierra.component :as component]))

(defrecord EndpointComponent [build-routes-fn]
  component/Lifecycle
  (start [component]
    (assoc component :routes (build-routes-fn component)))
  (stop [component]
    (dissoc component :routes)))

(defn endpoint-component [build-routes-fn]
  (->EndpointComponent build-routes-fn))
