(ns telegrammer-clj.component.worker
  (:require [clojure.core.async :as async]
            [com.stuartsierra.component :as component]))

(defrecord Worker
    [name input-chan worker-fn]
  component/Lifecycle
  (start [this]
    (println "Starting" name)
    (let [stop-chan (async/chan 1)]
      (async/go-loop []
        (async/alt!
          input-chan
          ([message]
           (try
             (worker-fn this message)
             (catch Throwable t
               (println "Error in" name t)))
           (recur))
          stop-chan
          ([_]
           :no-op)))
      (assoc this :stop-chan stop-chan)))
  (stop [this]
    (println "Stopping" name)
    (async/put! (:stop-chan this) :stop)
    (dissoc this :input-chan :stop-chan :result-chan :work-fn)))

(defn worker [name worker-fn]
  (map->Worker {:name name
                :worker-fn worker-fn}))
