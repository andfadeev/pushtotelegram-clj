(ns telegrammer-clj.component.handler-component
  (:require [com.stuartsierra.component :as component]
            [compojure.core :as compojure]))

(defn- find-endpoint-keys [component]
  (sort (map key (filter (comp :routes val) component))))

(defn- find-routes [component]
  (map #(:routes (get component %)) (find-endpoint-keys component)))

(defn- middleware-fn [component middleware]
  (if (vector? middleware)
    (let [[f & keys] middleware
          arguments  (map #(get component %) keys)]
      #(apply f % arguments))
    middleware))

(defn- compose-middleware [{:keys [middleware] :as component}]
  (->> (reverse middleware)
       (map #(middleware-fn component %))
       (apply comp identity)))

(defrecord Handler [middleware]
  component/Lifecycle
  (start [component]
    (println "Starting handler component")
    (let [routes (find-routes component)
          handler (compojure/routes (:routes (:site-endpoint component))
                                    (:routes (:webhook-endpoint component))
                                    (:routes (:api-endpoint component))
                                    )]
      (assoc component :handler handler)))
  (stop [component]
    (dissoc component :handler)))

(defn handler-component [options]
  (map->Handler options))
