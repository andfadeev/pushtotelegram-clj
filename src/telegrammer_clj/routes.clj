(ns telegrammer-clj.routes
  (:require [compojure.core :refer :all]))


(defn index-page []
  :index-page)

(defroutes site-routes
  (GET "/" [] (index-page)))

