(ns telegrammer-clj.system
  (:require [clojure.core.async :as async]
            [com.stuartsierra.component :as component]
            [meta-merge.core :refer [meta-merge]]
            [telegrammer-clj.worker.notification-worker :refer [notification-worker-fn]]
            [telegrammer-clj.worker.telegram-bot-worker :refer [telegram-bot-worker-fn]]
            [telegrammer-clj.worker.storage-worker :refer [storage-worker-fn]]
            [telegrammer-clj.component
             [endpoint-component :refer [endpoint-component]]
             [handler-component :refer [handler-component]]
             [hikaricp :refer [hikaricp]]
             [jetty-server :refer [jetty-server]]
             [worker :refer [worker]]]
            [telegrammer-clj.endpoint
             [api-endpoint :refer [api-endpoint]]
             [site-endpoint :refer [site-endpoint]]
             [webhook-endpoint :refer [webhook-endpoint]]]))

(def base-config {})

(defn build-system [config]
  (let [config (meta-merge config base-config)]
    (-> (component/system-map
         :telegram-bot-in-channel (async/chan 100)
         :notification-worker-in-channel (async/chan 100)
         :storage-worker-in-channel (async/chan 100)
         :site-handler (handler-component (:site-app config))
         :http (jetty-server (:http config))
         :webhook-endpoint (endpoint-component webhook-endpoint)
         :site-endpoint (endpoint-component site-endpoint)
         :api-endpoint (endpoint-component api-endpoint)
         :db (hikaricp (:db config))
         :telegram-bot (worker "telegram bot" telegram-bot-worker-fn)
         :notification-worker (worker "notification worker" notification-worker-fn)
         :storage-worker (worker "storage worker" storage-worker-fn))
        (component/system-using
         {:http [:site-handler]
          :site-handler [:site-endpoint :webhook-endpoint :api-endpoint]
          :site-endpoint {:db :db
                          :input-chan :notification-worker-in-channel}
          :api-endpoint {:db :db
                         :notif-chan :notification-worker-in-channel
                         :storage-chan :storage-worker-in-channel}
          :webhook-endpoint {:db :db
                             :channel :telegram-bot-in-channel}
          :telegram-bot {:input-chan :telegram-bot-in-channel}
          :notification-worker {:input-chan :notification-worker-in-channel
                                :result-chan :storage-worker-in-channel}
          :storage-worker {:input-chan :storage-worker-in-channel
                           :db :db}}))))
