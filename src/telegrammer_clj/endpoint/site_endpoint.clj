(ns telegrammer-clj.endpoint.site-endpoint
  (:require [buddy.auth
             [accessrules :refer [wrap-access-rules]]
             [middleware :as buddy-middleware]]
            [buddy.auth.backends.session :refer [session-backend]]
            [compojure
             [coercions :refer [as-int]]
             [core :refer :all]
             [route :refer [resources]]]
            [ring.middleware
             [anti-forgery :refer [wrap-anti-forgery]]
             [defaults :as defaults]
             [multipart-params :as mp]]
            [telegrammer-clj.auth :as auth-route :refer [on-error rules]]
            [telegrammer-clj.route
             [channel-route :as channel-route]
             [doc-route :refer [docs-page]]
             [index-route :as index-route]
             [message-route :as message-route]
             [tg-chat-route :as tg-chat-route]]
            [telegrammer-clj.route.auth
             [fb-route :as fb-route]
             [google-route :as google-route]]))

(def backend (session-backend))

(defn site-endpoint [{{db :spec} :db input-chan :input-chan}]
  (-> (routes
       (GET "/" [:as request]
         (index-route/index-page db request))
       (GET "/new-channel" [:as request]
         (channel-route/new-channel-page request))
       (GET "/my-channels" [:as request]
         (channel-route/my-channels-page db request))
       (GET "/channels" [:as request]
         (channel-route/channels-page db request))
       (GET "/channel/:id" [id :<< as-int :as request]
         (channel-route/channel-page db id request))
       (GET "/channel/:id/subscribe" [id :<< as-int :as request]
         (channel-route/subscribe db id request))
       (GET "/channel/:id/unsubscribe" [id :<< as-int :as request]
         (channel-route/unsubscribe db id request))
       (GET "/link-chat/:chat-id" [chat-id :<< as-int :as request]
         (tg-chat-route/link-account-to-chat db chat-id request))
       (GET "/linked" [:as request]
         (tg-chat-route/linked-page request))
       (GET "/already-linked" [:as r] (tg-chat-route/already-linked-page r))
       (GET "/docs" [:as request]
         (docs-page request))
       (POST "/send-message/:channel-id" [channel-id :<< as-int :as request]
         (message-route/send-message db input-chan channel-id request))
       (mp/wrap-multipart-params
        (POST "/create-channel" [:as request]
          (channel-route/create-channel db request)))
       (GET "/login" [next :as request]
         (auth-route/login-page next request))
       (GET "/logout" [:as request]
         (auth-route/logout request))
       (GET "/send-message/:channel-id" [channel-id :<< as-int :as r]
         (message-route/send-message-page db channel-id r))
       (context "/auth" []
         (GET "/fb" [next :as request]
           (fb-route/auth-fb next request))
         (GET "/fb_callback" [:as request]
           (fb-route/fb-callback db request))
         (GET "/google" [next :as request]
           (google-route/auth-google next request))
         (GET "/google_callback" [:as request]
           (google-route/google-callback db request)))
       (resources "/"))
      (wrap-routes (fn [handler]
                     (-> handler
                         (wrap-access-rules {:rules rules :on-error on-error})
                         (buddy-middleware/wrap-authorization backend)
                         (buddy-middleware/wrap-authentication backend)
                         (defaults/wrap-defaults defaults/site-defaults))))))

