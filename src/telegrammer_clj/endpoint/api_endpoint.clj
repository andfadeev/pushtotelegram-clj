(ns telegrammer-clj.endpoint.api-endpoint
  (:require [clojure.string :refer [blank?]]
            [compojure.api.sweet :refer [api context GET]]
            [schema.core :as s]
            [telegrammer-clj.route.api-route :as api-route]))

(s/defschema ApiChannelMessage {:key s/Str
                                :title s/Str
                                :text s/Str
                                (s/optional-key :link) (s/constrained s/Str (comp not blank?))})

(defn api-endpoint [{{db :spec} :db
                     notif-chan :notif-chan
                     storage-chan :storage-chan}]
  (api
   {:swagger {:ui "/api-docs"
              :spec "/swagger.json"
              :data {:info {:title "PushToTelegram Public API"}}
              }}
   (context "/api" []
     (GET "/send/channelMessage" []
       :return {:channel-id s/Int
                :recipients-count s/Int}
       :query [message ApiChannelMessage]
       (api-route/send-channel-message db notif-chan storage-chan message)))))


