(ns telegrammer-clj.endpoint.webhook-endpoint
  (:require [compojure.core :refer :all]
            [environ.core :refer [env]]
            [ring.middleware.json :refer [wrap-json-body]]
            [telegrammer-clj.route.telegram-webhook-route :as telegram-webhook-route]
            [ring.util.response :as ring-util]))

(defn webhook-endpoint [{{db :spec} :db channel :channel}]
  (-> (routes
       (POST (str "/telegram/" (env :telegram-token) "/webhook") [:as request]
         (telegram-webhook-route/telegram-webhook db channel request)))
      (wrap-routes wrap-json-body)))

