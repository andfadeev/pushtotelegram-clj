(ns telegrammer-clj.schema.message-schema-test
  (:require [telegrammer-clj.schema.message-schema :as sut]
            [clojure.test :refer :all]
            [schema.core :as s]))

(deftest channel-message-schema-test
  (testing "ChannelMessage with all fields ok"
    (is (nil? (s/check sut/Message {:type :channel-message
                                    :message {:title "Some title"
                                              :text "Some text"
                                              :link "http://www.somelink.com"}
                                    :channel-id 100
                                    :chat-ids #{1}}))))

  (testing "ChannelMessage with empty link is ok"
    (is (nil? (s/check sut/Message {:type :channel-message
                                    :message {:title "Some title"
                                              :text "Some text"}
                                    :channel-id 100
                                    :chat-ids #{1}}))))

  (testing "Missing channel-id"
    (is (= {:channel-id 'missing-required-key}
           (s/check sut/Message {:type :channel-message
                                 :message {:title "Some title"
                                           :text "Some text"}
                                 :chat-ids #{1}}))))
  )

