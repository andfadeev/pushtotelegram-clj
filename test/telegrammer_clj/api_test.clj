;; (ns telegrammer-clj.api-test
;;   (:require [clojure.test :as t]
;;             [clj-http.client :as http]))

;; (def api-key "9ec230606a8e48ac930e3674fad07fc3")

;; (comment
;;   (for [n (range 1 10)]
;;     (future
;;       (let [title (str "Title: " n)
;;             text (str "Text: " n)
;;             link (str "http://vk.com")]
;;         (http/get
;;          (format "http://alfa-pushtotelegram.rhcloud.com/api/sendMessage?key=9ec230606a8e48ac930e3674fad07fc3&type=channel&title=%s&text=%s&link=%s"
;;                  title text link)))))

;;   (http/get "http://alfa-pushtotelegram.rhcloud.com/api/sendMessage"
;;             {:query-params {"key" api-key
;;                             "type" "channel"
;;                             "title" "title"
;;                             "text" "text"
;;                             "link" "http://www.yandex.ru"}}))


(ns telegrammer-clj.api-test
  (:require [clj-http.client :as client]
            [clojure.test :refer :all]
            [com.stuartsierra.component :as component]
            [environ.core :refer [env]]
            [telegrammer-clj
             [config :as config]
             [system :as system]])
  (:import java.lang.Throwable))

(def test-config {:http {:port 3003}})

(def test-host (str "http://" (env :host) ":" (get-in test-config [:http :port])))

(def config (merge config/environ test-config))
(def test-api-key "2c87f0913180442893782ffde006933a")

(deftest test-system-creation
  (testing "Starting up system for tests"
    (let [system (system/build-system config)
          system (component/start system)]
      (try
        (is (= 200 (:status (client/get ))))

        (is (= 200 (:status (client/post (str test-host "/api/sendMessage?key=" test-api-key
                                              "&title=qwe&"
                                              )))))
        (is (= 200 (:status (client/post (str "http://" (env :host) ":3003"
                                              "/telegram/" (env :telegram-token) "/webhook")
                                         {:form-params {:message {:chat {:id 93410056}
                                                                  :text "/some-random-action"}}
                                          :content-type :json}))))
        (Thread/sleep 1000)
        (catch Throwable ignored))
      (component/stop system))))
