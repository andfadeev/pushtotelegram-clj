(ns telegrammer-clj.bot.telegram-bot-test
  (:require [clj-http.client :as client]
            [clojure.test :refer :all]
            [com.stuartsierra.component :as component]
            [environ.core :refer [env]]
            [telegrammer-clj
             [config :as config]
             [system :as system]])
  (:import java.lang.Throwable))

(def test-config {:http {:port 3003}})

(def config (merge config/environ test-config))

(deftest test-system-creation
  (testing "Starting up system for tests"
    (let [system (system/build-system config)
          system (component/start system)]
      (try
        (is (= 200 (:status (client/post (str "http://" (env :host) ":9001"
                                          "/telegram/" (env :telegram-token) "/webhook")
                                     {:form-params {:message {:chat {:id 93410056}
                                                              :text "/start"}}
                                      :content-type :json}))))
        (is (= 200 (:status (client/post (str "http://" (env :host) ":9001"
                                              "/telegram/" (env :telegram-token) "/webhook")
                                         {:form-params {:message {:chat {:id 93410056}
                                                                  :text "/some-random-action"}}
                                          :content-type :json}))))
        (Thread/sleep 1000)
        (catch Throwable ignored))
      (component/stop system))))
