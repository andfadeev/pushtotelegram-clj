(ns telegrammer-clj.core-test
  (:require [clojure.test :refer :all]
            [com.stuartsierra.component :as component]
            [environ.core :refer [env]]
            [telegrammer-clj
             [config :as config]
             [system :as system]]
            [telegrammer-clj.schema.message-schema :as message-schema]
            [clojure.core.async :as async]))

(def test-config {:http {:port 3002}})

(def config (merge config/environ test-config))

(deftest test-system-creation
  (testing "Starting up system for tests"
    (let [system (system/build-system config)
          system (component/start system)]
      (message-schema/put-telegram-webhook-message! (:in-channel system)
                                                    {:type :welcome-message
                                                     :chat-id 100})
      (is (= (:chat-id (async/<!! (:out-channel system))) 100))
      (component/stop system))))


