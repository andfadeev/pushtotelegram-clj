create sequence account_id_seq start 1;
--;;
create table account
(
  account_id integer primary key default nextval('account_id_seq'),
  name varchar(255) not null,
  image_url text not null,
  email varchar(255),
  reg_date timestamp not null,
  vk_id varchar(255) unique,
  google_id varchar(255) unique,
  fb_id varchar(255) unique
)
--;;
create sequence channel_id_seq start 100;
--;;
create table channel
(
  channel_id integer primary key default nextval('channel_id_seq'),
  name text not null,
  info text not null,
  links text,
  image_url text not null,
  api_key varchar(255) unique not null,
  is_private boolean default false,
  create_date timestamp not null,
  account_id integer references account (account_id)
)
--;;
create sequence message_id_seq start 100;
--;;
create table message
(
  message_id integer primary key default nextval('message_id_seq'),
  title text not null,
  text text not null,
  link text,
  type varchar(255) not null,
  create_date timestamp not null,
  channel_id integer references channel (channel_id)
)
--;;
create sequence subscriber_id_seq;
--;;
create table subscriber
(
  subscriber_id integer primary key default nextval('subscriber_id_seq'),
  channel_id integer references channel (channel_id),
  account_id integer references account (account_id)
)
--;;
create sequence tg_chat_id_seq;
--;;
create table tg_chat
(
  tg_chat_id integer primary key default nextval('tg_chat_id_seq'),
  chat_id integer not null,
  account_id integer references account (account_id)
)
