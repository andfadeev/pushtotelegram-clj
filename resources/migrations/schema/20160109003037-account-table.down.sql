drop table if exists message;
--;;
drop sequence if exists message_id_seq;
--;;
drop table if exists subscriber;
--;;
drop sequence if exists subscriber_id_seq;
--;;
drop table if exists channel;
--;;
drop sequence if exists channel_id_seq;
--;;
drop table if exists tg_chat;
--;;
drop sequence if exists tg_chat_id_seq;
--;;
drop table if exists account;
--;;
drop sequence if exists account_id_seq;


